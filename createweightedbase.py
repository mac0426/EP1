########################################################################
#           Simple random database creation DDL and DML                #
# (C) Copyright 2018 Decio Lauro Soares (deciolauro@gmail.com)         #
# This program is free software: you can redistribute it and/or modify #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# (at your option) any later version.                                  #
#                                                                      #
# This program is distributed in the hope that it will be useful,      #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public License    #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.#
########################################################################
import itertools
import random
import names

MAX_PEOPLE = 5000
MAX_FRIEND = 10
SEED = 9298281

random.seed(SEED)

try:
    # DDL Part
    f = open('DDL.sql', 'w')
    # Create entity people
    f.write('CREATE TABLE people (\n')
    f.write('    peo_id        integer,\n')
    f.write('    peo_name      varchar(80),\n')
    f.write('    CONSTRAINT pk_people PRIMARY KEY (peo_id)\n')
    f.write(');\n')
    f.write('\n')
    # Create relation relations
    f.write('CREATE TABLE relations (\n')
    f.write('    rel_peo_1     integer,\n')
    f.write('    rel_peo_2     integer,\n')
    f.write('    rel_weight    integer NOT NULL,\n')
    f.write('    CONSTRAINT pk_rel PRIMARY KEY (rel_peo_1, rel_peo_2),\n')
    f.write('    CONSTRAINT sk_rel1 FOREIGN KEY (rel_peo_1)\n')
    f.write('        REFERENCES people(peo_id),\n')
    f.write('    CONSTRAINT sk_rel2 FOREIGN KEY (rel_peo_2)\n')
    f.write('        REFERENCES people(peo_id)\n')
    f.write(');\n')
    f.write('\n')

    f.close()
    # Creating the random names array
    names_processed = 0
    used_names = []

    while names_processed < MAX_PEOPLE:
        n = names.get_full_name()
        if n not in used_names:
            used_names.append(n)
            names_processed += 1

    # DML Part
    g = open('DML.sql', 'w')

    # Populating people relation
    for i in range(MAX_PEOPLE):
        g.write("INSERT INTO people (peo_id, peo_name) VALUES ({0},'{1}');\n".format(i, used_names[i]))

    # Populating relations relationship
    for i in range(MAX_PEOPLE):
        num_friends = random.randint(0, MAX_FRIEND)
        possible_friends_iter = itertools.chain(range(i), range(i+1, MAX_PEOPLE))
        friends_of_i = random.sample(list(possible_friends_iter), num_friends)
        if len(friends_of_i) > 0:
            for j in range(len(friends_of_i)):
                g.write("INSERT INTO relations (rel_peo_1, rel_peo_2, rel_weight) VALUES ({0}, {1}, {2});\n".format(i, friends_of_i[j], random.randint(1, 10)))

    g.close()

except IOError as e:
    print("Something happened while creating the files, check for:")
    print(e)
except Exception as e:
    print("Unexpected error, check for:")
    print(e)
