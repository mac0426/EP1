from sys import argv
from neo4j.v1 import GraphDatabase
import folium


if len(argv) < 3:
    print('Usage: python3 q11.py <user> <password> [<query_limit>] [<host>] [<port>]')
    print('If ommitted, <host> <port> are assumed to be "localhost 7687".')
    print('<query_limit> ommitted or being 0 (zero) indicates no limit.')
    exit()

user = argv[1]
password = argv[2]

if len(argv) >= 4:
    limit = argv[3]
else:
    limit = '0'

if len(argv) >= 5:
    host = argv[4]
else:
    host = 'localhost'
    
if len(argv) >= 6:
    port = argv[5]
else:
    port = '7687'

driver = GraphDatabase.driver('bolt://' + host + ':' + port, auth=(user, password))
session = driver.session()

tx = session.begin_transaction()
result = tx.run("""
    match (n)
    where n.countries = "Brazil" OR n .country_codes = "BRA"
    call apoc.spatial.geocodeOnce(n.address) yield location
    return n.name, n.address, location """ + ('limit ' + limit if limit != '0' else ''))
tx.commit()

state_map = folium.Map(location=[-15.7801, -47.9292], zoom_start=4)
names = ['Acre','Alagoas','Amapá','Amazonas','Bahia','Ceará','Distrito Federal','Espírito Santo','Goiás','Maranhão',
         'Mato Grosso','Mato Grosso do Sul','Minas Gerais','Pará','Paraíba','Paraná','Pernambuco','Piauí',
         'Rio de Janeiro','Rio Grande do Norte','Rio Grande do Sul','Rondônia','Roraima','Santa Catarina','São Paulo',
         'Sergipe','Tocantins']
initials = ['AC','AL','AP','AM','BA','CE','DF','ES','GO','MA','MT','MS','MG','PA','PB','PR','PE','PI','RJ','RN','RS',
            'RO','RR','SC','SP','SE','TO']
counts = {'AC': 0,'AL': 0,'AP': 0,'AM': 0,'BA': 0,'CE': 0,'DF': 0,'ES': 0,'GO': 0,'MA': 0,'MT': 0,'MS': 0,'MG': 0,
          'PA': 0,'PB': 0,'PR': 0,'PE': 0,'PI': 0,'RJ': 0,'RN': 0,'RS': 0,'RO': 0,'RR': 0,'SC': 0,'SP': 0,'SE': 0,
          'TO': 0}

total_records = 0
for r in result.records():
    total_records += 1
    description = r['location']['description']
    latitude = r['location']['latitude']
    longitude = r['location']['longitude']
    
    folium.Marker([latitude, longitude], popup=r['n.name']).add_to(state_map)
    for i in range(len(names)):
        if description.find(names[i]) >= 0 or description.find(initials[i]) >= 0:
            counts[initials[i]] += 1
            break
            
for st in counts.keys():
    print(st + ':', counts[st], '({:.2%})'.format(counts[st] / total_records))

session.close()
state_map.save('map-q11.html')
